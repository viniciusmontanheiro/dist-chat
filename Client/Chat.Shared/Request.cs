﻿using System;
using System.Text;

namespace Chat.Shared
{
  public class Request
  {
    public Operator Operator { get; set; }
    public int OperandA { get; set; }
    public int OperandB { get; set; }
  }
}
