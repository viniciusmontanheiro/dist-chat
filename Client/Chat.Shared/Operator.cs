using System;
using System.Text;

namespace Chat.Shared
{

  public enum Operator : byte
  {
    Sum,
    Subtract,
    Multiply,
    Divide
  }
  
}
