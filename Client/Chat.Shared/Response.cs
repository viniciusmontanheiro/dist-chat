using System;
using System.Text;

namespace Chat.Shared
{

  public class Response
  {
    public int Result { get; set; }
    public bool Success { get; set; }
  }
  
}
